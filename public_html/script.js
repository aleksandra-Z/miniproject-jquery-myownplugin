$("document").ready(function(){
            $(".text").truncate();
});

(function($){
    
    $.fn.truncate = function(options){ //fn oznacza prototype, czyli na wszytskich obiekrtach te funcke mozna bedzie wywoływać
        
    var defaults ={
        more: "more...",
        less: "less..."
    }
    
//    $.extend(defaults, options); //to jest po to zeby uzytkownik mogl w swoim skrypcie juz podawac ustawienia (options nadpisują defaults, żeby nie nadpisywalu, trzeba uzyc zapisu ponizej) - $(.text)truncate(wiecej);
    
    var settings = $.extend({},defaults, options); //{} to pusty obiekt, nie ma nadpisywania
    
        
        return this.each(function(){ //each, dlatego, że ma to działać dla wsyztskich obiektoww
                var obj = $(this);
                var objText = $(this).html();
                var textLength = objText.length;
                if(textLength>200){
                    var breakingPoint = objText.indexOf(" ",200);
                    var visiblePart = objText.substring(0,breakingPoint);
                    var hiddenPart = objText.substring(breakingPoint, textLength);
                    
                    obj.html("<span class='visible'>"+visiblePart+"</span><span class='more'>"+settings.more+"</span><span class='hidden'>"+hiddenPart+"</span><span class='less'>"+settings.less+"</span>");
                    
                    $(".more", obj).click(function(){
                        $(".more", obj).toggle(200);
                        $(".less", obj).toggle(200);
                        $(".hidden", obj).toggle(200);
                    });
                    $(".less", obj).click(function(){
                        $(".more", obj).toggle(200);
                        $(".less", obj).toggle(200);
                        $(".hidden", obj).toggle(200);
                    });
            }
        });
    }
           
            
})(jQuery); //taki zapis funkcji jest po to, żeby wywoływała się zawsze na początku